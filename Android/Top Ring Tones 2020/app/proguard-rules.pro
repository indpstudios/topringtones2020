# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepattributes EnclosingMethod

-keep class indpstudios.ganeshringtones.ModelClass.** { *; }

-keepattributes *Annotation*
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
-ignorewarnings
-keep class * {
    public private *;
}
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}


-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}
-dontwarn org.apache.commons.**
-dontwarn com.google.**
-dontwarn com.j256.ormlite**
-dontwarn org.apache.http**

-keepattributes SourceFile,LineNumberTable
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }

-keepattributes Signature
# GSON Library
# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

-keepattributes SourceFile, LineNumberTable

# Google Map
-keep class com.google.android.gms.maps.** { *; }
-keep interface com.google.android.gms.maps.** { *; }

-keep class org.apache.harmony.awt.** { *; }
-dontwarn org.apache.harmony.awt.**

-keep class com.github.siyamed.** { *; }
-dontwarn com.github.siyamed.**

-keep class com.squareup.picasso.** { *; }
-dontwarn com.squareup.picasso.**

-keep class com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

-keep class com.sun.mail.** { *; }
-dontwarn com.sun.mail.**

-keep class org.codehaus.mojo.** { *; }
-dontwarn org.codehaus.mojo.**

-keep class java.awt.datatransfer.** { *; }
-dontwarn java.awt.datatransfer.**

-keep class java.nio.file.** { *; }
-dontwarn java.nio.file.**

-keep class javax.mail.** { *; }
-dontwarn javax.mail.**

-keep class com.theartofdev.edmodo.** { *; }
-dontwarn com.theartofdev.edmodo.**

-keep class javax.activation.** { *; }
-dontwarn javax.activation.**

-dontskipnonpubliclibraryclassmembers

