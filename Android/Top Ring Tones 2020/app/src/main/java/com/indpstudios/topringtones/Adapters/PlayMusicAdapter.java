package com.indpstudios.topringtones.Adapters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.indpstudios.topringtones.ModelClass.FMSubDataMain;
import com.indpstudios.topringtones.ProjectUtils.PlayMusicListener;
import com.indpstudios.topringtones.ProjectUtils.StopPlayingListener;
import com.indpstudios.topringtones.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class PlayMusicAdapter extends RecyclerView.Adapter<PlayMusicAdapter.MyViewHolder> {
    private ArrayList<FMSubDataMain> musicDetailsList;
    private Context context;
    private PlayMusicListener playMusicListener;
    private StopPlayingListener stopPlayingListener;
    private InterstitialAd RingAdds;
    private InterstitialAd AlarmAdds;
    private InterstitialAd NotAdds;
    private int SelectedPosition = 0;

    public PlayMusicAdapter(Context context, ArrayList<FMSubDataMain> dashBoardList, PlayMusicListener listener1, StopPlayingListener listener2) {
        this.context = context;
        this.playMusicListener = listener1;
        this.stopPlayingListener = listener2;
        this.musicDetailsList = dashBoardList;

        RingAdds = new InterstitialAd(context);
        RingAdds.setAdUnitId(context.getResources().getString(R.string.FullScreenId));
        RingAdds.loadAd(new AdRequest.Builder().build());

        RingAdds.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                try {
                    ChangeRingtone(musicDetailsList.get(SelectedPosition).getRawFile(), musicDetailsList.get(SelectedPosition).getMusicUrl(), musicDetailsList.get(SelectedPosition).getHeaderName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RingAdds.loadAd(new AdRequest.Builder().build());
            }
        });

        AlarmAdds = new InterstitialAd(context);
        AlarmAdds.setAdUnitId(context.getResources().getString(R.string.FullScreenId));
        AlarmAdds.loadAd(new AdRequest.Builder().build());

        AlarmAdds.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                try {
                    ChangeAlarm(musicDetailsList.get(SelectedPosition).getRawFile(), musicDetailsList.get(SelectedPosition).getMusicUrl(), musicDetailsList.get(SelectedPosition).getHeaderName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                AlarmAdds.loadAd(new AdRequest.Builder().build());
            }
        });

        NotAdds = new InterstitialAd(context);
        NotAdds.setAdUnitId(context.getResources().getString(R.string.FullScreenId));
        NotAdds.loadAd(new AdRequest.Builder().build());

        NotAdds.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                try {
                    ChangeNotification(musicDetailsList.get(SelectedPosition).getRawFile(), musicDetailsList.get(SelectedPosition).getMusicUrl(), musicDetailsList.get(SelectedPosition).getHeaderName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                NotAdds.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.samplecut_row_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            final FMSubDataMain dashBoard = musicDetailsList.get(position);
            holder.Id_MusicName.setText(dashBoard.getHeaderName());
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(holder.Id_MusicGif);
            Glide.with(context).load(R.raw.musicgif).into(imageViewTarget);
            holder.Id_MusicGif.setVisibility(View.GONE);
            holder.Id_PauseButton.setVisibility(View.GONE);

            holder.Id_PlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playMusicListener.StartPlaying(position);
                }
            });
            holder.Id_PauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopPlayingListener.StopPlaying(position);
                    holder.Id_PlayButton.setVisibility(View.VISIBLE);
                    holder.Id_PauseButton.setVisibility(View.GONE);
                    holder.Id_MusicGif.setVisibility(View.GONE);
                    holder.Id_MoreOptions.setVisibility(View.VISIBLE);
                }
            });
            if (position == mSelectedPosition) {
                holder.Id_MoreOptions.setVisibility(View.GONE);
                holder.Id_MusicGif.setVisibility(View.VISIBLE);
                holder.Id_PlayButton.setVisibility(View.GONE);
                holder.Id_PauseButton.setVisibility(View.VISIBLE);
            } else {
                holder.Id_PlayButton.setVisibility(View.VISIBLE);
                holder.Id_PauseButton.setVisibility(View.GONE);
                holder.Id_MusicGif.setVisibility(View.GONE);
                holder.Id_MoreOptions.setVisibility(View.VISIBLE);
            }

            holder.Id_MoreOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // setSelectedItem(position);
                    ShowPopupForLogout(v, position);
                }
            });

            holder.Id_MusicName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // setSelectedItem(position);
                    ShowPopupForLogout(v, position);
                }
            });
            //  animate(holder);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean saveAs(int resSoundId, String FileName, String FolderName) {
        byte[] buffer = null;
        InputStream fIn = context.getResources().openRawResource(resSoundId);
        int size = 0;

        try {
            size = fIn.available();
            buffer = new byte[size];
            fIn.read(buffer);
            fIn.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return false;
        }
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + FolderName + "/";
        String filename = FileName + ".mp3";
        boolean exists = (new File(path)).exists();
        if (!exists) {
            new File(path).mkdirs();
        }
        FileOutputStream save;
        try {
            save = new FileOutputStream(path + filename);
            save.write(buffer);
            save.flush();
            save.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return false;
        }
        return true;
    }

    private void ChangeRingtone(int Rawfile, String Filenmae, String TitleName) {
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Ringtones" + "/" + Filenmae + ".mp3");
            if (file.exists()) {
                if (file != null) {
                    ChangeRingtone(file, TitleName);
                    Log.e("File Path", "Already Existes");
                }
            } else {
                if (saveAs(Rawfile, Filenmae, "Ringtones")) {
                    File filenew = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Ringtones" + "/" + Filenmae + ".mp3");
                    if (filenew.exists()) {
                        ChangeRingtone(filenew, TitleName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeAlarm(int Rawfile, String Filenmae, String TitleName) {
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Alarms" + "/" + Filenmae + ".mp3");
            if (file.exists()) {
                if (file != null) {
                    ChangeAlarm(file, TitleName);
                    Log.e("File Path", "Already Existes");
                }
            } else {
                if (saveAs(Rawfile, Filenmae, "Alarms")) {
                    File filenew = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Alarms" + "/" + Filenmae + ".mp3");
                    if (filenew.exists()) {
                        ChangeAlarm(filenew, TitleName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeNotification(int Rawfile, String Filenmae, String TitleName) {
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Notifications" + "/" + Filenmae + ".mp3");
            if (file.exists()) {
                if (file != null) {
                    ChangeNotification(file, TitleName);
                    Log.e("File Path", "Already Existes");
                }
            } else {
                if (saveAs(Rawfile, Filenmae, "Notifications")) {
                    File filenew = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "Notifications" + "/" + Filenmae + ".mp3");
                    if (filenew.exists()) {
                        ChangeNotification(filenew, TitleName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeRingtone(File file, String TitleName) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
            values.put(MediaStore.MediaColumns.TITLE, TitleName); //You will have to populate
            values.put(MediaStore.MediaColumns.SIZE, 215454);
            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
            values.put(MediaStore.Audio.Media.ARTIST, context.getResources().getString(R.string.app_name)); //You will have to populate this
            values.put(MediaStore.Audio.Media.DURATION, 230);
            values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
            values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
            values.put(MediaStore.Audio.Media.IS_ALARM, true);

            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            contentResolver.delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file.getAbsolutePath() + "\"", null);
            Uri newUri = contentResolver.insert(uri, values);
            RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, newUri);


            Toast.makeText(context, TitleName + " is set as your default RingTone", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeAlarm(File file, String TitleName) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
            values.put(MediaStore.MediaColumns.TITLE, TitleName);
            values.put(MediaStore.MediaColumns.SIZE, 215454);
            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
            values.put(MediaStore.Audio.Media.ARTIST, context.getResources().getString(R.string.app_name));
            values.put(MediaStore.Audio.Media.DURATION, 230);
            values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
            values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
            values.put(MediaStore.Audio.Media.IS_ALARM, true);


            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            contentResolver.delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file.getAbsolutePath() + "\"", null);
            Uri newUri = contentResolver.insert(uri, values);
            RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM, newUri);

            Toast.makeText(context, TitleName + " is set as your default Alarm Tone", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeNotification(File file, String TitleName) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
            values.put(MediaStore.MediaColumns.TITLE, TitleName);
            values.put(MediaStore.MediaColumns.SIZE, 215454);
            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
            values.put(MediaStore.Audio.Media.ARTIST, context.getResources().getString(R.string.app_name));
            values.put(MediaStore.Audio.Media.DURATION, 230);
            values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
            values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
            values.put(MediaStore.Audio.Media.IS_ALARM, true);


            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            contentResolver.delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file.getAbsolutePath() + "\"", null);
            Uri newUri = contentResolver.insert(uri, values);
            RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION, newUri);


            Toast.makeText(context, TitleName + " is set as your default Notification Tone", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ShowPopupForLogout(View view, final int position) {
        try {
            SelectedPosition = position;
            PopupMenu popup = new android.support.v7.widget.PopupMenu(context, view);
            popup.getMenuInflater().inflate(R.menu.menu_playitem, popup.getMenu());
            final Menu popupMenu = popup.getMenu();

            popup.setOnMenuItemClickListener(new android.support.v7.widget.PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    int id = item.getItemId();
                    if (id == R.id.Id_SetRingtone) {
                        try {
                            if (RingAdds.isLoaded()) {
                                RingAdds.show();
                            } else {
                                ChangeRingtone(musicDetailsList.get(position).getRawFile(), musicDetailsList.get(position).getMusicUrl(), musicDetailsList.get(position).getHeaderName());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    } else if (id == R.id.Id_SetAlarm) {
                        try {
                            if (AlarmAdds.isLoaded()) {
                                AlarmAdds.show();
                            } else {
                                ChangeAlarm(musicDetailsList.get(position).getRawFile(), musicDetailsList.get(position).getMusicUrl(), musicDetailsList.get(position).getHeaderName());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    } else if (id == R.id.Id_SetNotification) {
                        try {
                            if (NotAdds.isLoaded()) {
                                NotAdds.show();
                            } else {
                                ChangeNotification(musicDetailsList.get(position).getRawFile(), musicDetailsList.get(position).getMusicUrl(), musicDetailsList.get(position).getHeaderName());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                    return true;
                }
            });

            /*********For Loading Popupmenu Icons***********/
            try {
                java.lang.reflect.Field[] fields = popup.getClass().getDeclaredFields();
                for (java.lang.reflect.Field field : fields) {
                    if ("mPopup".equals(field.getName())) {
                        field.setAccessible(true);
                        Object menuPopupHelper = field.get(popup);
                        Class<?> classPopupHelper = Class
                                .forName(menuPopupHelper.getClass().getName());
                        Method setForceIcons = classPopupHelper
                                .getMethod("setForceShowIcon", boolean.class);
                        setForceIcons.invoke(menuPopupHelper, true);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            popup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return musicDetailsList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageButton Id_PlayButton, Id_PauseButton, Id_MoreOptions;
        private ImageView Id_MusicGif;
        private TextView Id_MusicName;
        public View itemView;

        public MyViewHolder(View view) {
            super(view);
            Id_MusicName = (TextView) view.findViewById(R.id.Id_HeaderName);
            Id_PlayButton = (ImageButton) view.findViewById(R.id.Id_PlayButton);
            Id_PauseButton = (ImageButton) view.findViewById(R.id.Id_PauseButton);
            Id_MoreOptions = (ImageButton) view.findViewById(R.id.Id_MoreOptions);
            Id_MusicGif = (ImageView) view.findViewById(R.id.Id_MusicGif);
            itemView = view;
        }
    }

    private int mSelectedPosition = -1;

    public void setSelectedItem(int itemPosition) {
        mSelectedPosition = itemPosition;
        notifyDataSetChanged();
    }
}
