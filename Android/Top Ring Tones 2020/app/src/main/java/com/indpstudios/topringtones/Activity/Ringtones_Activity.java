package com.indpstudios.topringtones.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.indpstudios.topringtones.Adapters.PlayMusicAdapter;
import com.indpstudios.topringtones.ModelClass.FMSubDataMain;
import com.indpstudios.topringtones.ProjectUtils.PlayMusicListener;
import com.indpstudios.topringtones.ProjectUtils.ProjectMethods;
import com.indpstudios.topringtones.ProjectUtils.StopPlayingListener;
import com.indpstudios.topringtones.R;

import java.util.ArrayList;


public class Ringtones_Activity extends AppCompatActivity implements PlayMusicListener, StopPlayingListener {
    private RecyclerView Id_MainRecyclerView;
    private ArrayList<FMSubDataMain> mainDetailsArrayList;
    private PlayMusicAdapter dashBoardAdapter;
    private int[] RawFilesArray = {
            R.raw.a1,
            R.raw.a2,
            R.raw.a3,
            R.raw.a4,
            R.raw.a5,
            R.raw.a6,
            R.raw.a7,
            R.raw.a8,
            R.raw.a9,
            R.raw.a10,
            R.raw.a11,
            R.raw.a12,
            R.raw.a13,
            R.raw.a14,
            R.raw.a15,
            R.raw.a16,
            R.raw.a17,
            R.raw.a18,
            R.raw.a19,
            R.raw.a20,
            R.raw.a21,
            R.raw.a22,
            R.raw.a23,
            R.raw.a24,
            R.raw.a25,
            R.raw.a26,
            R.raw.a27,
            R.raw.a28,
            R.raw.a29,
            R.raw.a30,
            R.raw.a31,
            R.raw.a32,
            R.raw.a33,
            R.raw.a34,
            R.raw.a35,
            R.raw.a36,
            R.raw.a37,
            R.raw.a38,
            R.raw.a39,
            R.raw.a40,
            R.raw.a41,
            R.raw.a42,
            R.raw.a43,
            R.raw.a44,
            R.raw.a45,
            R.raw.a46,
            R.raw.a47,
            R.raw.a48,
            R.raw.a49,
            R.raw.a50,
            R.raw.a51,
            R.raw.a52,
            R.raw.a53,
            R.raw.a54,
            R.raw.a55,
            R.raw.a56,
            R.raw.a57,
            R.raw.a58,
            R.raw.a59,
            R.raw.a60,
            R.raw.a61,
            R.raw.a62,
            R.raw.a63,
            R.raw.a64,
            R.raw.a65,
            R.raw.a66,
            R.raw.a67,
            R.raw.a68,
            R.raw.a69,
            R.raw.a70,
            R.raw.a71,
            R.raw.a72,
            R.raw.a73,
            R.raw.a74,
            R.raw.a75,
            R.raw.a76,
            R.raw.a77,
            R.raw.a78,
            R.raw.a79,
            R.raw.a80,
            R.raw.a81,
            R.raw.a82,
            R.raw.a83,
            R.raw.a84,
            R.raw.a85,
            R.raw.a86,
            R.raw.a87,
            R.raw.a88


    };
    private String[] MusicNamesArray = {
            "a1",
            "a2",
            "a3",
            "a4",
            "a5",
            "a6",
            "a7",
            "a8",
            "a9",
            "a10",
            "a11",
            "a12",
            "a13",
            "a14",
            "a15",
            "a16",
            "a17",
            "a18",
            "a19",
            "a20",
            "a21",
            "a22",
            "a23",
            "a24",
            "a25",
            "a26",
            "a27",
            "a28",
            "a29",
            "a30",
            "a31",
            "a32",
            "a33",
            "a34",
            "a35",
            "a36",
            "a37",
            "a38",
            "a39",
            "a40",
            "a41",
            "a42",
            "a43",
            "a44",
            "a45",
            "a46",
            "a47",
            "a48",
            "a49",
            "a50",
            "a51",
            "a52",
            "a53",
            "a54",
            "a55",
            "a56",
            "a57",
            "a58",
            "a59",
            "a60",
            "a61",
            "a62",
            "a63",
            "a64",
            "a65",
            "a66",
            "a67",
            "a68",
            "a69",
            "a70",
            "a71",
            "a72",
            "a73",
            "a74",
            "a75",
            "a76",
            "a77",
            "a78",
            "a79",
            "a80",
            "a81",
            "a82",
            "a83",
            "a84",
            "a85",
            "a86",
            "a87",
            "a88",
            "a89",
            "a90"



    };
    private String[] HeaderNamesArray = {
            "Ring Tone1",
            "Ring Tone2",
            "Ring Tone3",
            "Ring Tone4",
            "Ring Tone5",
            "Ring Tone6",
            "Ring Tone7",
            "Ring Tone8",
            "Ring Tone9",
            "Ring Tone10",
            "Ring Tone11",
            "Ring Tone12",
            "Ring Tone13",
            "Ring Tone14",
            "Ring Tone15",
            "Ring Tone16",
            "Ring Tone17",
            "Ring Tone18",
            "Ring Tone19",
            "Ring Tone20",
            "Ring Tone21",
            "Ring Tone22",
            "Ring Tone23",
            "Ring Tone24",
            "Ring Tone25",
            "Ring Tone26",
            "Ring Tone27",
            "Ring Tone28",
            "Ring Tone29",
            "Ring Tone30",
            "Ring Tone31",
            "Ring Tone32",
            "Ring Tone33",
            "Ring Tone34",
            "Ring Tone35",
            "Ring Tone36",
            "Ring Tone37",
            "Ring Tone38",
            "Ring Tone39",
            "Ring Tone40",
            "Ring Tone41",
            "Ring Tone42",
            "Ring Tone43",
            "Ring Tone44",
            "Ring Tone45",
            "Ring Tone46",
            "Ring Tone47",
            "Ring Tone48",
            "Ring Tone49",
            "Ring Tone50",
            "Ring Tone51",
            "Ring Tone52",
            "Ring Tone53",
            "Ring Tone54",
            "Ring Tone55",
            "Ring Tone56",
            "Ring Tone57",
            "Ring Tone58",
            "Ring Tone59",
            "Ring Tone60",
            "Ring Tone61",
            "Ring Tone62",
            "Ring Tone63",
            "Ring Tone64",
            "Ring Tone65",
            "Ring Tone66",
            "Ring Tone67",
            "Ring Tone68",
            "Ring Tone69",
            "Ring Tone70",
            "Ring Tone71",
            "Ring Tone72",
            "Ring Tone73",
            "Ring Tone74",
            "Ring Tone75",
            "Ring Tone76",
            "Ring Tone77",
            "Ring Tone78",
            "Ring Tone79",
            "Ring Tone80",
            "Ring Tone81",
            "Ring Tone82",
            "Ring Tone83",
            "Ring Tone84",
            "Ring Tone85",
            "Ring Tone86",
            "Ring Tone87",
            "Ring Tone88",
            "Ring Tone89",
            "Ring Tone90",


    };
    private PlayMusicListener playMusicListener;
    private StopPlayingListener stopPlayingListener;
    private static final int REQUEST_WRITE_PERMISSION = 1001;
    private int ItemCurrentposition = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_all_stations);
            playMusicListener = this;
            stopPlayingListener = this;
            mainDetailsArrayList = new ArrayList<>();
            Id_MainRecyclerView = (RecyclerView) findViewById(R.id.Id_MainRecyclerView);
            checkSystemWritePermission();
            checkPermission();
            LoadGoogleAdds();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            if (ProjectMethods.mediaPlayer != null) {

            } else {
                ProjectMethods.mediaPlayer = new MediaPlayer();
            }
            FMRadioListData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadGoogleAdds() {
        MobileAds.initialize(this, getString(R.string.AppId));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public void PlayMusicMethod() {
        try {
            StopMusic();
            StartMusic();
            dashBoardAdapter.setSelectedItem(ItemCurrentposition);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void StopMusic() {
        try {
            if (ProjectMethods.mediaPlayer != null) {
                ProjectMethods.mediaPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void StartMusic() {
        try {
            String uriPath = "android.resource://" + getPackageName() + "/raw/" + mainDetailsArrayList.get(ItemCurrentposition).getMusicUrl();
            Uri uri = Uri.parse(uriPath);
            ProjectMethods.mediaPlayer.reset();
            //  final AssetFileDescriptor sound = getResources().openRawResourceFd(animalsound);
            ProjectMethods.mediaPlayer.setDataSource(Ringtones_Activity.this, uri);
            ProjectMethods.mediaPlayer.setLooping(true);
            ProjectMethods.mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            ProjectMethods.mediaPlayer.prepareAsync();
            ProjectMethods.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            ProjectMethods.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            //   dashBoardAdapter.setSelectedItem(ItemCurrentposition);
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong..!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void OpenSystemSettings() {
        try {
            new AlertDialog.Builder(Ringtones_Activity.this)
                    .setTitle("Permission Required")
                    .setMessage("Access to system permission is required to set ringtone.")
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    openAndroidPermissionsMenu();
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkSystemWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(Ringtones_Activity.this))
                return true;
            else
                OpenSystemSettings();
        }
        return false;
    }

    private void openAndroidPermissionsMenu() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
            ) {
                String[] permissions = new String[]{
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_SETTINGS,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                };
                requestPermissions(permissions, 100);
            }
        }
    }

    private void FMRadioListData() {
        mainDetailsArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < RawFilesArray.length; i++) {
                FMSubDataMain obj_dish = new FMSubDataMain();
                obj_dish.setHeaderName(HeaderNamesArray[i]);
                obj_dish.setRawFile(RawFilesArray[i]);
                obj_dish.setMusicUrl(MusicNamesArray[i]);
                mainDetailsArrayList.add(obj_dish);
            }
            if (mainDetailsArrayList.size() > 0) {
                dashBoardAdapter = new PlayMusicAdapter(Ringtones_Activity.this, mainDetailsArrayList, playMusicListener, stopPlayingListener);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Ringtones_Activity.this);
                Id_MainRecyclerView.setLayoutManager(mLayoutManager);
                Id_MainRecyclerView.setHasFixedSize(true);
                int resId = R.anim.layout_animation_down_to_up;
                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(Ringtones_Activity.this, resId);
                Id_MainRecyclerView.setLayoutAnimation(animation);
                Id_MainRecyclerView.setItemAnimator(new DefaultItemAnimator());
                Id_MainRecyclerView.setAdapter(dashBoardAdapter);
            } else {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (ProjectMethods.mediaPlayer != null) {
                    ProjectMethods.mediaPlayer.stop();
                }
                finish();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            int i = 0;
            for (int grant : grantResults) {
                if (grant == PackageManager.PERMISSION_DENIED) {
                    i++;
                }
            }
            if (i > 0) {
                showDialog(0);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (ProjectMethods.mediaPlayer != null) {
            ProjectMethods.mediaPlayer.stop();
        }
        finish();
    }

    @Override
    public void StartPlaying(int position) {
        ItemCurrentposition = position;
        PlayMusicMethod();
    }

    @Override
    public void StopPlaying(int position) {
        ItemCurrentposition = position;
        StopMusic();
    }
}
