package com.indpstudios.topringtones.ProjectUtils;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.format.Time;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.TimePicker;

import com.indpstudios.topringtones.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ProjectMethods {
    public static int timervalue = 0;
    public static MediaPlayer mediaPlayer;

    public static boolean haveNetworkConnection(Context context) {
        boolean Result = false;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                Result = true;
            } else {
                Result = false;
            }
        }
        return Result;
    }


    public static boolean CheckEnableGPS(Context context) {
        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static String GetFirstDate() {
        String fromdate = null;
        try {
            final Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, 1);
            SimpleDateFormat ss = new SimpleDateFormat("dd-MM-yyyy");
            fromdate = ss.format(c.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fromdate;
    }

    public static String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(c.getTime());
    }

    static int blendColors(int from, int to, float ratio) {
        final float inverseRation = 1f - ratio;
        final float r = Color.red(from) * ratio + Color.red(to) * inverseRation;
        final float g = Color.green(from) * ratio + Color.green(to) * inverseRation;
        final float b = Color.blue(from) * ratio + Color.blue(to) * inverseRation;
        return Color.rgb((int) r, (int) g, (int) b);
    }

    public static String GetCurrentTime() {
        String CurrentTime = "";
        Time now = new Time();
        String hour = "";
        String minute = "";
        try {
            now.setToNow();
            hour = String.valueOf(now.hour);
            minute = String.valueOf(now.minute);

        } catch (NumberFormatException nfe) {

        }
        return (hour + ":" + minute);
    }

    public static String GetTime(Context context, TextView textView) {
        final String[] SelectedTime = {""};
        TimePickerDialog mTimePicker;
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String time = selectedHour + ":" + selectedMinute;
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                String SelectedTime = fmtOut.format(date);
                textView.setText(SelectedTime);
            }
        }, hour, minute, false);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
        return SelectedTime[0];
    }


    public static String SetZerosBeforeInteger(String IntgerValue, int TargetStringLength) {
        try {
            for (int i = 1; i <= TargetStringLength; i++) {
                if (IntgerValue.length() < TargetStringLength) {
                    IntgerValue = "0" + IntgerValue;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return IntgerValue;
    }

    public static Cursor Execute_rawQuery(String Qry, SQLiteDatabase minfodb) {
        synchronized ("dblock") {
            Cursor mCursor = null;
            try {
                mCursor = minfodb.rawQuery(Qry, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mCursor;
        }
    }

    public static void OPenSCreenAnimation(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

}
