package com.indpstudios.topringtones.ProjectUtils;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.ads.MobileAds;
import com.indpstudios.topringtones.R;

public class MyApplication extends Application {

    private static Context context;
    private static MyApplication mInstance;

    public static Context getGlobalContext() {
        return context;
    }

    @Override
    public void onCreate() {
        context = getApplicationContext();
        mInstance = this;
        MobileAds.initialize(this, getString(R.string.AppId));
        super.onCreate();
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

}